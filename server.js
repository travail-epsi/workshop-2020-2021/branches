const process = require("process");
const express = require("express");
const errorHandler = require("./middleware/error");

const app = express();

var apiRouter = require("./routes/index");


app.use("/", apiRouter);
app.use(errorHandler);

const port = process.env.PORT;

const server = app.listen(port, () => {
    let host = server.address().address;
    host = host === "::" ? "localhost" : host;
    console.log("2: We are live on " + host + ":" + port);
});


module.exports = app;